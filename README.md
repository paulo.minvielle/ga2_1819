## Unity Engine

Unity est un moteur généraliste permettant de créer des applications interactives temps réel multiplatformes. Il fournit de nombreuses fonctionnalités de base nécessaires au bon développement d'applications modernes, permettant ainsi un gain de temps considérable durant le développement. Cependant, il est faux de penser que cela rends le développement de jeux "facile" car Unity fournit des fonctionnalités générales (sprites/modèles, son, animations, particules, etc...) qui ne sont en fait que la matière première. Tout le développement est à faire, rien n'est magique.

Quelques jeux récents développés sous Unity
* [Hearthstone (2015)](https://playhearthstone.com/fr-fr/)
* [Firewatch (2016)](https://store.steampowered.com/app/383870/Firewatch/)
* [Ori and the blind forest (2016)](https://store.steampowered.com/app/387290/Ori_and_the_Blind_Forest_Definitive_Edition/)
* [The Long Dark (2017)](https://store.steampowered.com/app/305620/The_Long_Dark/)
* [Cuphead (2017)](https://store.steampowered.com/app/268910/Cuphead/)
* [The Last Night (2018)](https://store.steampowered.com/app/612400/The_Last_Night/)
* [Ghost of a tale (2018)](https://store.steampowered.com/app/417290/Ghost_of_a_Tale/)

**Bibliographie**  
* [Site web](https://unity3d.com)
* [Manuel](https://docs.unity3d.com/Manual/index.html)
* [API](https://docs.unity3d.com/ScriptReference/index.html)
* [Tutoriels Officiels](https://unity3d.com/fr/learn/tutorials)


## C# (C Sharp)

Le C# est un langage de programmation orienté objet, commercialisé par [Microsoft](https://fr.wikipedia.org/wiki/Microsoft) depuis 2002. C'est un langage dérivé du C/C++ ressemblant au Java, fait pour le développement sur la plateforme [Microsoft .NET](https://fr.wikipedia.org/wiki/Microsoft_.NET) (prononcé dot net). Cette dernière est le nom donnée à un ensemble de technologies de Microsoft permettant de rendre les applications facilement portables sur diverses platformes.

**Bibliographie**  
* [Tutoriels C# Unity](https://unity3d.com/fr/learn/tutorials/s/scripting)
* [Guide C# Microsoft](https://docs.microsoft.com/fr-fr/dotnet/csharp/programming-guide/)
* [Brackeys' C# course](https://www.youtube.com/watch?v=pSiIHe2uZ2w&list=PLPV2KyIb3jR6ZkG8gZwJYSjnXxmfPAl51&index=1)