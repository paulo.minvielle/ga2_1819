# Introduction au C#
Le C# (proncé si-sharp) dans Unity possède quelques différences par rapport au C# classique, nous commencerons tout d'abord par nous intéresser au C# puis nous verrons comment l'utiliser dans Unity.

## Que peut-ont faire en C#
Quasiment tout. C'est un langage très versatile, multiplateforme, utilisé autant pour les applications desktop que pour le web ou le mobile. Il est aussi possible de l'utiliser en tant que langage de script (comme le fait Unity) en embarquant mono.

## Vocabulaire
**Console application:** programme executable depuis une ligne de commande.  
**IDE**: Integrated development environment (Visual Studio, Monodevelop, etc...)  

## Créer un projet console
![UnityInstallation](./imgs/create_csharp_project.gif)

## Le programme par défaut

Il est inutile de vouloir tout comprendre du premier coup, c'est impossible. Il faut y aller par étape en prenant chaque chose l'une après l'autre.  
On s'intéressera surtout à ce qu'il se passe dans le corps de la fonction `Main`.

```c#
using System;

namespace MyFirstApp {
    class Program {
        // Ceci est une méthode nommée "Main", automatiquement appelée lors de l'éxecution du programme.
        // C'est le point d'entrée.
        static void Main(string[] args) {
            // Ceci est un appel de la fonction Console.WriteLine() qui permet d'afficher quelque chose dans la console.
            // Le paramètre passé est la chaîne de caractère "Hello world !", c'est donc ce qui y sera affiché.
            Console.WriteLine("Hello world !");
        }
    }
}

```

En exécutant le programme sur windows, la console s'ouvre puis se ferme automatiquement car le programme fini de s'exécuter très rapidement. Pour éviter que cela arrive, nous allons rajouter une ligne qui attends un entrée clavier afin de mettre en pause l'exécution.

```c#
Console.ReadKey();
```

> **Tip**
> Dans une chaîne de caractère, vous pouvez utiliser `\n` pour effectuer un retour à la line.

> **Exercice**  
> Afficher votre prénom, attendre une entrée clavier, afficher votre age et enfin attendre une entrée clavier avant de terminer l'exécution.
