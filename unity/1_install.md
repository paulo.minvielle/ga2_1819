## Unity
* [Créez un compte Unity](https://id.unity.com/en/conversations/33a7282c-c214-4d28-9e96-ac4a257af2f9009f)
* [Téléchargez et installez Unity 2018.2.7](https://unity3d.com/fr/get-unity/download?thank-you=update&download_nid=58241&os=Win)

_Options à sélectionner lors de l'installation._  
![UnityInstallation](./imgs/install.png)