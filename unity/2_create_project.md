Lancez Unity, connectez-vous et créez un nouveau projet.

![CreateProject](./imgs/create_project.png)

## Anatomie d'un projet
Une fois le projet créé, vous trouverez un dossier avec le contenu ci-dessous.  

![UnityFolder](./imgs/project_folder.png)

| Nom                   | Obligatoire | Description                                                                                                             |
|-----------------------|-------------|-------------------------------------------------------------------------------------------------------------------------|
| Assets                | Oui         | Contient les ressources de votre application, que vous pouvez voir dans l'onglet "Project" de l'interface               |
| Library               | Non         | Contient des fichiers générés par Unity lors du premier lancement pour accélérer les chargements                        |
| Packages              | Oui         | Contient les informations nécessaire à Unity pour télécharger les packages optionnels ajoutés depuis le Package Manager |
| ProjectSettings       | Oui         | Les options de votre projet (Qualité,                                                                                   |
| Temp                  | Non         | Contient des fichiers générés temporaires                                                                               |
| Autres (.sln, etc...) | Non         | Fichiers de solutions Visual Studio (ou autre éditeurs)                                                                 |

> Lorsqu'il faudra créer un ZIP de votre projet pour le partager, évitez d'inclure les dossiers non obligatoires car ils sont souvent volumineux `(surtout Library !)`.
